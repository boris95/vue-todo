import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        todos: [
            { "completed": false,
                "id": 201,
                "title": "Standup meeting with the team @5pm",
                "description": null,
                "inDate": "Jul 10",
                "priority": "Low",
                "tags": [{ "name": "Personal", "color": "rgb(102, 204, 255)" }],
                "priorityColor": "#5e72e4"
            },

            { "completed": true,
                "id": 202,
                "title": "Order pizza for Granny tonight", "description": "Thin crust medium size roasted chicken",
                "inDate": "Jul 10",
                "priority": "High",
                "tags": [{ "name": "Home", "color": "rgb(255, 203, 0)" }, { "name": "Personal", "color": "rgb(102, 204, 255)" }],
                "priorityColor": "#f5365c" },

            { "completed": false,
                "id": 203,
                "title": "Design, Develop and Deploy Apps to Netlify for Clients",
                "description": null,
                "inDate": "Jul 10",
                "priority": "Medium",
                "tags": [{ "name": "Home", "color": "rgb(255, 203, 0)" }],
                "priorityColor": "#ffbb33"
            }
        ],
        allTags: [
            {
                name: "Home",
                color: "rgb(255, 203, 0)"
            },
            {
                name: "Personal",
                color: "rgb(102, 204, 255)"
            },
            {
                name: "Work",
                color: "rgb(162, 93, 220)"
            }
        ],
        defaultTagColor: "rgb(128, 128, 128)",
        colorPalete: [
            "#00C821",
            "#9CD326",
            "#CAB641",
            "#FFCB00",
            "#784BD1",
            "#A25DDC",
            "#0086C0",
            "#579BFC",
            "#66CCFF",
            "#BB3354",
            "#E2445C",
            "#FF158A",
            "#FF5AC4",
            "#FF642E",
            "#FDAB3D",
            "#7F5347",
            "#C4C4C4",
            "#808080",
            "#333333"
        ],
    },
    mutations: {
        ADD_NEW_TAG(state, tagName) {
            state.allTags.push({ name: tagName, color: state.defaultTagColor })
        },
        CREATE_NEW_TODO(state, payload) {
            state.todos.unshift(payload)
        },
        MARK_AS_COMPLETE(state, key) {
            state.todos[key].completed = !state.todos[key].completed
        },
        DELETE_TODO(state, key) {
            state.todos.splice(key, 1);
        },
        CHANGE_TAG_COLOR: (state, payload) => state.allTags[payload.key].color = payload.color,

        LOAD_DATA(state, payload){
            state.todos = payload;
        },
        LOAD_TAGS(state, payload){
            state.allTags = payload;
        },
    },
    actions: {
        createNewTodo({ commit}, payload) {
            commit("CREATE_NEW_TODO", payload)
        },
        markAsComplete({commit}, payload) {
            commit("MARK_AS_COMPLETE", payload)
        },
        changeTagColor({commit}, tag) {
            commit('CHANGE_TAG_COLOR', tag);
            this.dispatch('saveTagsToLocalStorage')
        },
         addNewTag({commit}, payload) {
            commit("ADD_NEW_TAG", payload)
             this.dispatch('saveTagsToLocalStorage')
        },
        deleteTodo({commit}, payload) {
            commit("DELETE_TODO", payload)
            // this.dispatch('saveDataToLocalStorage')
        },
        loadDataFromLocalStorage({commit}) {
            if (localStorage.getItem('todos')) {
                try {
                    const parsed = JSON.parse(localStorage.getItem('todos'));
                    commit("LOAD_DATA", parsed)
                    // console.log('ToDo Json Loaded')
                } catch(e) {
                    localStorage.removeItem('todos');
                    console.log('Error parse ToDo Json')
                }
            }
        },
        saveDataToLocalStorage() {
            const parsed = JSON.stringify(this.state.todos);
            localStorage.setItem('todos', parsed);
            // console.log('Json saved to LocalStorage')
        },
        saveTagsToLocalStorage(){
            const parsed = JSON.stringify(this.state.allTags);
            localStorage.setItem('allTags', parsed);
        },
        loadTagsFromLocalStorage({commit}){
            if (localStorage.getItem('allTags')) {
                try {
                    const parsed = JSON.parse(localStorage.getItem('allTags'));
                    commit("LOAD_TAGS", parsed)
                    // console.log('ToDo Json Loaded')
                } catch(e) {
                    localStorage.removeItem('allTags');
                    console.log('Error parse ToDo Json')
                }
            }
        }
    },
    getters:{
        filterTags: state => {
            return (id) => state.allTags.filter(tag => {
                if (tag)
                    return tag.name.toLowerCase().includes(id.toLowerCase());
            });
        },
        colorPalete: state => state.colorPalete,
        getAllTags: state => state.allTags,
        getTodos: state => state.todos,
        getCompletedTodos: state => state.todos.reduce(function(sum, elem) {
            return elem.completed ? sum + 1 : sum
        }, 0),
        getPendingTodos: state => state.todos.reduce(function(sum, elem) {
                return !elem.completed ? sum + 1 : sum
            }, 0),
        getMaxId: state => state.todos.reduce((max, item) => item.id > max ? item.id : max, 0),
        getTodoById: state => id => {
            return state.todos.find(todo => todo.id === id);
        }

    },

})

