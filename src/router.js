import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
      // {
      //   path: '/',
      //   name: 'home',
      //   component: Home
      // },
        {
            path: '/',
            name: 'todo',
            component: () => import(/* webpackChunkName: "about" */ './components/Todo.vue')
        },

        {
            path: '/todo/:id',
            name: 'todoDetail',
            component: () => import(/* webpackChunkName: "about" */ './components/TodoDetail.vue')
        },
        {
            path: '/about',
            name: 'about',
            component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
        }

  ]
})
